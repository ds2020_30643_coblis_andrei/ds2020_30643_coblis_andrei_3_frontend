import http from "./httpService";
import { apiUrl } from "../config.json";

const apiEndpoint = apiUrl + "/caregivers";

function caregiverUrl(id) {
  return `${apiEndpoint}/${id}`;
}

export function getCaregivers() {
  return http.get(apiEndpoint);
}

export function getCaregiver(id) {
  return http.get(caregiverUrl(id));
}

export function saveCaregiver(caregiver) {
  if (caregiver.id) {
    const body = { ...caregiver };
    delete body.id;
    return http.put(caregiverUrl(caregiver.id), body);
  }

  return http.post(apiEndpoint, caregiver);
}

export function deleteCaregiver(id) {
  return http.delete(caregiverUrl(id));
}
