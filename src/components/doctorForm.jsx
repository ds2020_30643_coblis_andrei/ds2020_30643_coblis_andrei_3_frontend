import React from "react";
import Joi from "joi-browser";
import Form from "./common/form";
import { getDoctor, saveDoctor } from "../services/doctorService";

class DoctorForm extends Form {
  state = {
    data: {
      userId: ""
    },
    errors: {}
  };

  schema = {
    id: Joi.number(),
    userId: Joi.string()
      .required()
      .label("UserId")
  };

  async populateDoctor() {
    try {
      const id = this.props.match.params.id;
      if (id === "new") return;

      const { data: doctor } = await getDoctor(id);
      this.setState({ data: this.mapToViewModel(doctor) });
    } catch (ex) {
      if (ex.response && ex.response.status === 404)
        this.props.history.replace("/not-found");
    }
  }

  async componentDidMount() {
    await this.populateDoctor();
  }

  mapToViewModel(doctor) {
    return {
      id: doctor.id,
      userId: doctor.userId
    };
  }

  doSubmit = async () => {
    await saveDoctor(this.state.data);

    this.props.history.push("/Doctors");
  };

  render() {
    return (
      <div>
        <h1>Doctor Form</h1>
        <form onSubmit={this.handleSubmit}>
          {this.renderInput("userId", "UserId")}
          {this.renderButton("Save")}
        </form>
      </div>
    );
  }
}

export default DoctorForm;
