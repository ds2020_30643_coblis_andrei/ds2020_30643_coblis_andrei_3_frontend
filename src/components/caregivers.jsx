import React, { Component } from "react";
import { Link } from "react-router-dom";
import { toast } from "react-toastify";
import Pagination from "./common/pagination";
import CaregiversTable from "./caregiversTable";
import { getCaregivers, deleteCaregiver } from "../services/caregiverService";
import { paginate } from "../utils/paginate";
import _ from "lodash";
import SearchBox from "./searchBox";

class Caregivers extends Component {
    state = {
        caregivers: [],
        currentPage: 1,
        pageSize: 4,
        searchQuery: "",
        sortColumn: { path: "username", order: "asc" }
    }

    async componentDidMount() {
        const { data: caregivers } = await getCaregivers();
        this.setState({ caregivers });
    }

    handleDelete = async caregiver => {
        const originalCaregivers = this.state.caregivers;
        const caregivers = originalCaregivers.filter(u => u.id !== caregiver.id);
        this.setState({ caregivers });
    
        try {
          await deleteCaregiver(caregiver.id);
        } catch (ex) {
          if (ex.response && ex.response.status === 404)
            toast.error("This caregiver has already been deleted.");
    
          this.setState({ caregivers: originalCaregivers });
        }
    };

    handlePageChange = page => {
        this.setState({ currentPage: page });
    };

    handleSearch = query => {
        this.setState({ searchQuery: query, currentPage: 1 });
    };

    handleSort = sortColumn => {
        this.setState({ sortColumn });
    };

    getPagedData = () => {
        const {
          pageSize,
          currentPage,
          sortColumn,
          searchQuery,
          caregivers: allCaregivers
        } = this.state;
    
        let filtered = allCaregivers;
        if (searchQuery)
          filtered = allCaregivers.filter(u =>
            u.username.toLowerCase().startsWith(searchQuery.toLowerCase())
          );
    
        const sorted = _.orderBy(filtered, [sortColumn.path], [sortColumn.order]);
    
        const caregivers = paginate(sorted, currentPage, pageSize);
    
        return { totalCount: filtered.length, data: caregivers };
    };

    render() {
        const { length: count } = this.state.caregivers;
        const { pageSize, currentPage, sortColumn, searchQuery } = this.state;
        //const { user } = this.props;
    
        if (count === 0) return <p>There are no caregivers in the database.</p>;
    
        const { totalCount, data: caregivers } = this.getPagedData();
    
        return (
          <div className="row">
            <div className="col">
              {
                <Link
                  to="/caregivers/new"
                  className="btn btn-primary"
                  style={{ marginBottom: 20 }}
                >
                  New Caregiver
                </Link>
              }
              <p>Showing {totalCount} caregivers in the database.</p>
              <SearchBox value={searchQuery} onChange={this.handleSearch} />
              <CaregiversTable
                caregivers={caregivers}
                sortColumn={sortColumn}
                onDelete={this.handleDelete}
                onSort={this.handleSort}
              />
              <Pagination
                itemsCount={totalCount}
                pageSize={pageSize}
                currentPage={currentPage}
                onPageChange={this.handlePageChange}
              />
            </div>
          </div>
        );
    }

}
export default Caregivers;